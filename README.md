# FourthwallTestProject



## Info

1. I have implemented all basic 4 features stated in requirements.]
2. No paging and 3 columns of pictures in a landscape mode implemented though but I've left hooks so it might be easily to do so.
3. It took me +- 10 hours but I have to admit that figuring out how to load and scroll smoothly images using Kingfisher library took me 3 hours (I've never used this library before).
4. Basic configuration for unit tests has been implemented
5. I've used Kingfisher as a Swift Package Dependency - I hope once the project is cloned into your repository it will work out of the box (Kingfisher dependecy will be working properly).

If you have any questions write me: tomasz.milczarek@hey.com, +48604505308

Best regards,
Tomasz Milczarek

