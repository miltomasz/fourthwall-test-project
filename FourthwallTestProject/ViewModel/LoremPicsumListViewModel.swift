//
//  LoremPicsumListViewModel.swift
//  FourthwallTestProject
//
//  Created by Tomasz Milczarek on 05/12/2021.
//

import Foundation

extension LoremPicsumListViewModel {
    
    // MARK: - Input
    
    struct LoremPicsumMainViewModelInput {
        var loadPics: (() -> Void)?
        var selectPhotoAction: ((String) -> Void)?
    }
    
    // MARK: - Output

    struct LoremPicsumMainViewModelOutput {
        var onError: ((Error) -> Void)?
        var onPicsLoaded: (([Photo]) -> Void)?
        var onSelectedPhoto: ((String) -> Void)?
    }
    
}

final class LoremPicsumListViewModel: ViewModelType {
    
    var input: LoremPicsumMainViewModelInput = .init()
    var output: LoremPicsumMainViewModelOutput = .init()
    
    // MARK: - Properties
    
    private var page: Int = 0
    private var apiClient: ClientAPIProtocol
    
    // MARK: - Initialization
    
    init(page: Int = 0, apiClient: ClientAPIProtocol) {
        self.page = page
        self.apiClient = apiClient
        setupInputBinding()
    }
    
    // MARK: - Setup
    
    private func setupInputBinding() {
        input.loadPics = { [weak self] in
            guard let self = self else { return }
            
            self.loadPics(page: self.page)
        }
        
        input.selectPhotoAction = { [weak self] photoId in
            self?.output.onSelectedPhoto?(photoId)
        }
    }
    
    // MARK: - Methods
    
    private func loadPics(page: Int) {
        apiClient.getPhotos(page: page) { [weak self] photos, error  in
            guard let self = self, let photos = photos else { return }
            
            if let error = error {
                self.output.onError?(error)
            } else {
                self.output.onPicsLoaded?(photos)
            }
        }
    }
    
}
