//
//  ViewModelType.swift
//  FourthwallTestProject
//
//  Created by Tomasz Milczarek on 03/12/2021.
//

import Foundation

public protocol ViewModelType: class {
    associatedtype Input
    associatedtype Output
    
    var input: Input { get }
    var output: Output { get }
}
