//
//  PhotoDetailsViewModel.swift
//  FourthwallTestProject
//
//  Created by Tomasz Milczarek on 08/12/2021.
//

import Foundation

extension PhotoDetailsViewModel {
    
    // MARK: - Input
    
    struct PhotoDetailsViewModelInput {
        var loadPhoto: (() -> Void)?
    }
    
    // MARK: - Output

    struct PhotoDetailsViewModelOutput {
        var onError: ((Error) -> Void)?
        var onPhotoLoaded: ((Photo) -> Void)?
    }
    
}

final class PhotoDetailsViewModel: ViewModelType {
    
    var input: PhotoDetailsViewModelInput = .init()
    var output: PhotoDetailsViewModelOutput = .init()
    
    // MARK: - Properties
    
    private var apiClient: ClientAPIProtocol
    private let photoId: String
    
    // MARK: - Initialization
    
    init(photoId: String, apiClient: ClientAPIProtocol = LoremPicsumAPIClient()) {
        self.photoId = photoId
        self.apiClient = apiClient
        setupInputBinding()
    }
    
    // MARK: - Setup
    
    private func setupInputBinding() {
        input.loadPhoto = { [weak self] in
            guard let self = self else { return }
            
            self.loadPhoto(id: self.photoId)
        }
    }
    
    // MARK: - Methods
    
    private func loadPhoto(id: String) {
        apiClient.getPhoto(id: id) { [weak self] photo, error  in
            guard let self = self, let photo = photo else { return }
            
            if let error = error {
                self.output.onError?(error)
            } else {
                self.output.onPhotoLoaded?(photo)
            }
        }
    }

}

