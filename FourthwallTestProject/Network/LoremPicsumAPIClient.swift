//
//  LoremPicsumAPIClient.swift
//  FourthwallTestProject
//
//  Created by Tomasz Milczarek on 05/12/2021.
//

import Foundation

struct LoremPicsumAPIClient: ClientAPIProtocol {
    
    enum Endpoints {
        static let basePhotosUrl = "https://picsum.photos/"
        static let photosUrl = basePhotosUrl + "v2/list?page="
        static let photoUrl = basePhotosUrl + "id/"
        
        case getPhotos(page: Int)
        case getPhoto(id: String)
        
        private var stringValue: String {
            switch self {
            case let .getPhotos(page):
                return Endpoints.photosUrl + "\(page)"
            case let .getPhoto(id):
                return Endpoints.photoUrl + "\(id)/info"
            }
        }
        
        var url: URL {
            return URL(string: stringValue)!
        }
    }
    
    // MARK: GET requests
    
    private static func taskForGETRequest<ResponseType: Decodable>(url: URL, responseType: ResponseType.Type, completion: @escaping (ResponseType?, Error?) -> Void) -> URLSessionDataTask {
        
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data else {
                DispatchQueue.main.async {
                    completion(nil, error)
                }
                return
            }
            
            let responseObject = decodeJson(from: data, type: ResponseType.self)
            
            DispatchQueue.main.async {
                completion(responseObject, nil)
            }
        }
        
        task.resume()
        
        return task
    }
    
    private static func decodeJson<ResponseType: Decodable>(from data: Data, type: ResponseType.Type) -> ResponseType? {
        let decoder = JSONDecoder()
        do {
            let responseObject = try decoder.decode(type, from: data)
            return responseObject
        } catch let error {
            debugPrint("Decode json error: \(error)")
            return nil
        }
    }
    
    // MARK: Protocol methods
    
    func getPhotos(page: Int = 0, completion: @escaping ([Photo]?, Error?) -> Void) {
        let _ = Self.taskForGETRequest(url: Endpoints.getPhotos(page: page).url, responseType: [Photo].self) { photos, error in
            if let photos = photos {
                completion(photos, nil)
            } else {
                completion(nil, error)
            }
        }
    }
    
    func getPhoto(id: String, completion: @escaping (Photo?, Error?) -> Void) {
        let _ = Self.taskForGETRequest(url: Endpoints.getPhoto(id: id).url, responseType: Photo.self) { photo, error in
            if let photo = photo {
                completion(photo, nil)
            } else {
                completion(nil, error)
            }
        }
    }
    
}
