//
//  NetworkHelper.swift
//  FourthwallTestProject
//
//  Created by Tomasz Milczarek on 05/12/2021.
//

import UIKit

struct NetworkHelper {
    
    // MARK: - Private initialization
    
    private init() {}
    
    // MARK: - Methods
    
    static func showLoader(_ show: Bool, activityIndicator: UIActivityIndicatorView) {
        if show {
            activityIndicator.isHidden = false
            activityIndicator.startAnimating()
        } else {
            activityIndicator.stopAnimating()
            activityIndicator.isHidden = true
        }
    }
    
    static func showFailurePopup(title: String, message: String, on viewController: UIViewController) {
        let alertViewController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertViewController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        
        viewController.show(alertViewController, sender: nil)
    }
    
}
