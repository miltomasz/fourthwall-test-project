//
//  ClientAPIProtocol.swift
//  FourthwallTestProject
//
//  Created by Tomasz Milczarek on 08/12/2021.
//

import Foundation

protocol ClientAPIProtocol {
    func getPhotos(page: Int, completion: @escaping ([Photo]?, Error?) -> Void)
    func getPhoto(id: String, completion: @escaping (Photo?, Error?) -> Void)
}
