//
//  PhotoViewCell.swift
//  FourthwallTestProject
//
//  Created by Tomasz Milczarek on 06/12/2021.
//

import UIKit

final class PhotoViewCell: UICollectionViewCell {
    
    // MARK: - Properties
    
    var imageView: UIImageView = UIImageView()
    
    // MARK: - Initialization
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        setupImageView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()

        imageView.image = nil
    }
    
    // MARK: - Setup
    
    private func setupImageView() {
        addSubview(imageView)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            imageView.widthAnchor.constraint(equalTo: widthAnchor),
            imageView.heightAnchor.constraint(equalTo: heightAnchor)
        ])
        
        imageView.contentMode = .scaleToFill
    }
    
}
