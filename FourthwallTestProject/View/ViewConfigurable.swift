//
//  ViewConfigurable.swift
//  FourthwallTestProject
//
//  Created by Tomasz Milczarek on 03/12/2021.
//

import UIKit

public typealias ViewConfigurable = ViewContainable & ViewLoadable

public protocol ViewContainable {
    associatedtype ContentViewType: UIView
    var containableView: ContentViewType { get }
}

public extension ViewContainable where Self: UIViewController {
    var containableView: ContentViewType {
        guard let view = view as? ContentViewType else { fatalError("Please implement `loadView()` with proper view type in \(type(of: self))") }
        return view
    }
}

public protocol ViewLoadable {
    associatedtype ContentViewType: UIView
    func loadContentView()
}

public extension ViewLoadable where Self: UIViewController {
    func loadContentView() {
        view = ContentViewType()
    }
}
