//
//  PhotoDetailsView.swift
//  FourthwallTestProject
//
//  Created by Tomasz Milczarek on 08/12/2021.
//

import UIKit
import Kingfisher

final class PhotoDetailsView: UIView {
    
    // MARK: - View
    
    private let imageView: UIImageView = Subviews.imageView
    
    // MARK: - Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSelf()
        setupLayout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Setup
    
    private func setupSelf() {
        backgroundColor = .white
    }
    
    private func setupLayout() {
        setupImageView()
    }
    
    private func setupImageView() {
        addSubview(imageView)
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            imageView.widthAnchor.constraint(equalTo: widthAnchor),
            imageView.heightAnchor.constraint(equalTo: heightAnchor)
        ])
    }
    
    func setupViewWithModel(model: Photo) {
        let urlString = model.download_url
        let imageUrl = URL(string: urlString)
        let processor = DownsamplingImageProcessor(size: imageView.bounds.size)
        imageView.kf.indicatorType = .activity
        
        if ImageCache.default.isCached(forKey: urlString) {
            ImageCache.default.retrieveImage(forKey: urlString) { [weak self] result in
                switch result {
                case .success(let value):
                    self?.imageView.kf.setImage(with: imageUrl, placeholder: value.image)
                case .failure(let error):
                    debugPrint("Error while loading photo from cache: \(error)")
                }
            }
        } else {
            imageView.kf.setImage(with: imageUrl,
                                  options: [
                                    .processor(processor),
                                    .loadDiskFileSynchronously,
                                    .scaleFactor(UIScreen.main.scale),
                                    .transition(.fade(1))
                                  ])
        }
    }
    
}

// MARK: - Subviews

private enum Subviews {
    
    static var imageView: UIImageView {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        return imageView
    }
    
}
