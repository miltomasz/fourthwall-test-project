//
//  LoremPicsumListView.swift
//  FourthwallTestProject
//
//  Created by Tomasz Milczarek on 03/12/2021.
//

import UIKit
import Kingfisher

final class LoremPicsumListView: UIView {

    // MARK: - View
    
    private let collectionView: UICollectionView = Subviews.collectionView
    private let activityIndicator: UIActivityIndicatorView = Subviews.activityIndicator
   
    // MARK: - Variables
    
    private var photos: [Photo]?
    var onSelectedPhotoAction: ((String) -> Void)?
    var columnsPerRow: Int = 2
    
    // MARK: - Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSelf()
        setupLayout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Setup
    
    private func setupSelf() {
        backgroundColor = .white
    }
    
    private func setupLayout() {
        setupCollectionView()
        setupActivityIndicator()
    }
    
    private func setupCollectionView() {
        addSubview(collectionView)
        
        NSLayoutConstraint.activate([
            collectionView.widthAnchor.constraint(equalTo: widthAnchor),
            collectionView.heightAnchor.constraint(equalTo: heightAnchor)
        ])
        
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.prefetchDataSource = self
    }
    
    private func setupActivityIndicator() {
        addSubview(activityIndicator)
        
        NSLayoutConstraint.activate([
            activityIndicator.centerXAnchor.constraint(equalTo: centerXAnchor),
            activityIndicator.centerYAnchor.constraint(equalTo: centerYAnchor)
        ])
    }
    
    func setupViewWithModel(model: [Photo]) {
        self.photos = model
        collectionView.reloadData()
    }
    
    // MARK: - Methods
    
    func startActivityIndicator() {
        NetworkHelper.showLoader(true, activityIndicator: activityIndicator)
    }
    
    func stopActivityIndicator() {
        NetworkHelper.showLoader(false, activityIndicator: activityIndicator)
    }
    
}

// MARK: - UICollectionViewDataSource

extension LoremPicsumListView: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoViewCellId", for: indexPath) as? PhotoViewCell, let photos = photos else { return PhotoViewCell() }
        
        let urlString = photos[indexPath.row].download_url
        let imageUrl = URL(string: urlString)
        let processor = DownsamplingImageProcessor(size: CGSize(width: 200, height: 200))
        cell.imageView.kf.indicatorType = .activity
        
        if ImageCache.default.isCached(forKey: urlString) {
            ImageCache.default.retrieveImage(forKey: urlString) { result in
                switch result {
                case .success(let value):
                    cell.imageView.kf.setImage(with: imageUrl, placeholder: value.image)
                case .failure(let error):
                    debugPrint("Error while loading from cache: \(error)")
                }
            }
        } else {
            cell.imageView.kf.setImage(with: imageUrl,
                                       options: [
                                        .processor(processor),
                                        .loadDiskFileSynchronously,
                                        .scaleFactor(UIScreen.main.scale),
                                        .transition(.fade(1))
                                       ])
        }
        return cell
    }
      
}

extension LoremPicsumListView: UICollectionViewDataSourcePrefetching {
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
        
        for indexPath in indexPaths {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoViewCellId", for: indexPath) as? PhotoViewCell, let photos = photos else { return }
            
            let urlString = photos[indexPath.row].download_url
            
            guard !ImageCache.default.isCached(forKey: urlString) else { continue }
            
            let imageUrl = URL(string: urlString)
            let processor = DownsamplingImageProcessor(size: CGSize(width: 200, height: 200))
            cell.imageView.kf.setImage(with: imageUrl,
                                       options: [
                                        .processor(processor),
                                        .loadDiskFileSynchronously,
                                        .scaleFactor(UIScreen.main.scale),
                                        .transition(.fade(1)),
                                       ])
            
        }
    }
}

// MARK: - UICollectionViewDelegate

extension LoremPicsumListView: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let photos = photos else { return }
        
        let photoId = photos[indexPath.row].id
        
        onSelectedPhotoAction?(photoId)
    }
    
}

// MARK: - UICollectionViewDelegateFlowLayout

extension LoremPicsumListView: UICollectionViewDelegateFlowLayout {
    
    private enum Configuration {
        static let sectionInsets = UIEdgeInsets(top: 50.0, left: 20.0, bottom: 50.0, right: 20.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let paddingSpace = Configuration.sectionInsets.left * CGFloat(columnsPerRow + 1)
        let availableWidth = frame.width - paddingSpace
        let widthPerItem = availableWidth / CGFloat(columnsPerRow)
        
        return CGSize(width: widthPerItem, height: widthPerItem)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return Configuration.sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return Configuration.sectionInsets.left
    }
    
}

// MARK: - Subviews

private enum Subviews {
    
    static var collectionView: UICollectionView {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        collectionView.register(PhotoViewCell.self, forCellWithReuseIdentifier: "PhotoViewCellId")
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.collectionViewLayout = collectionViewFlowLayout
        collectionView.backgroundColor = .clear
        return collectionView
    }
    
    static var collectionViewFlowLayout: UICollectionViewFlowLayout {
        let collectionViewFlowLayout = UICollectionViewFlowLayout()
        return collectionViewFlowLayout
    }
    
    static var activityIndicator: UIActivityIndicatorView {
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.hidesWhenStopped = true
        activityIndicator.style = .large
        return activityIndicator
    }
    
}
