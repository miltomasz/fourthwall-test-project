//
//  PhotoDetailsViewController.swift
//  FourthwallTestProject
//
//  Created by Tomasz Milczarek on 08/12/2021.
//

import UIKit

final class PhotoDetailsViewController: UIViewController, ViewConfigurable {
    
    // MARK: - View
    
    typealias ContentViewType = PhotoDetailsView
    
    // MARK: - ViewModel
    
    private let viewModel: PhotoDetailsViewModel
    
    // MARK: - Initialization
    
    init(viewModel: PhotoDetailsViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Lifecycle
    
    override func loadView() {
        loadContentView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupOutputBinding()
        loadPhoto()
    }
    
    // MARK: - Setup
    
    private func loadPhoto() {
        viewModel.input.loadPhoto?()
    }
    
    private func setupOutputBinding() {
        viewModel.output.onPhotoLoaded = { [weak self] photo in
            guard let self = self else { return }
            
            self.title = photo.author
            self.containableView.setupViewWithModel(model: photo)
        }
        
        viewModel.output.onError = { [weak self] error in
            guard let self = self else { return }
            
            NetworkHelper.showFailurePopup(title: "Error", message: error.localizedDescription, on: self)
        }
    }

}
