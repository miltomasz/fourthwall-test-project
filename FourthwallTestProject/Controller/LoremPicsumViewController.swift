//
//  ViewController.swift
//  FourthwallTestProject
//
//  Created by Tomasz Milczarek on 03/12/2021.
//

import UIKit

final class LoremPicsumViewController: UIViewController, ViewConfigurable {
    
    // MARK: - View
    
    typealias ContentViewType = LoremPicsumListView
    
    // MARK: - ViewModel
    
    private let viewModel: LoremPicsumListViewModel
    
    // MARK: - Initialization
    
    init(viewModel: LoremPicsumListViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Lifecycle
    
    override func loadView() {
        loadContentView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSelf()
        setupInputBinding()
        setupOutputBinding()
        loadData()
    }
    
    // MARK: - Setup
    
    private func setupSelf() {
        title = "FourthwallTestProject"
    }
    
    private func loadData() {
        containableView.startActivityIndicator()
        viewModel.input.loadPics?()
    }
    
    private func setupInputBinding() {
        containableView.onSelectedPhotoAction = { [weak self] photoId in
            self?.viewModel.input.selectPhotoAction?(photoId)
        }
    }
    
    private func setupOutputBinding() {
        viewModel.output.onPicsLoaded = { [weak self] model in
            guard let self = self else { return }
            
            self.containableView.stopActivityIndicator()
            self.containableView.setupViewWithModel(model: model)
        }
        
        viewModel.output.onSelectedPhoto = { [weak self] photoId in
            let photoDetailsViewController = PhotoDetailsViewController(viewModel: PhotoDetailsViewModel(photoId: photoId))
            self?.navigationController?.pushViewController(photoDetailsViewController, animated: true)
        }
        
        viewModel.output.onError = { [weak self] error in
            guard let self = self else { return }
            
            NetworkHelper.showFailurePopup(title: "Error", message: error.localizedDescription, on: self)
        }
    }

}

