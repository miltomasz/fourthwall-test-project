//
//  LoremPicsumListViewModelTests.swift
//  FourthwallTestProject
//
//  Created by Tomasz Milczarek on 08/12/2021.
//

import XCTest

class LoremPicsumListViewModelTests: XCTestCase {
    
    var sut: LoremPicsumListViewModel!
    
    override func setUpWithError() throws {
        sut = LoremPicsumListViewModel(page: 0, apiClient: LoremPicsumAPIClicentMock())
    }
    
    override func tearDownWithError() throws {
        sut = nil
    }
    
    func testGetPhotos() {
        let expectation = XCTestExpectation(description: "Returns correct photo")
        let expectedPhoto = Photo(id: "123", author: "TestAuthor", width: 100, height: 200, url: "", download_url: "")
        
        sut.output.onPicsLoaded = { photos in
            XCTAssertEqual(photos[0].id, expectedPhoto.id)
            expectation.fulfill()
        }
        
        sut.input.loadPics?()
    }
    
}

struct LoremPicsumAPIClicentMock: ClientAPIProtocol {
    
    func getPhoto(id: String, completion: @escaping (Photo?, Error?) -> Void) {
        completion(nil, nil)
    }
    
    func getPhotos(page: Int = 0, completion: @escaping ([Photo]?, Error?) -> Void) {
        let photo = Photo(id: "123", author: "TestAuthor", width: 100, height: 200, url: "", download_url: "")
        completion([photo], nil)
    }
    
}
